#!/usr/bin/env python3
"""Encrypt or decrypt a file.

Uses Openssl to encrypt and decrypt.
"""
import subprocess
from argparse import ArgumentParser
from getpass import getpass
from os import path
import hashlib

DEVE = 'development'
PROD = 'production'
STAG = 'staging'
ENVIRONMENTS = [DEVE, STAG, PROD, ]

ENCRYPT = 'encrypt'
DECRYPT = 'decrypt'

SHA256 = 'sha256'
OPENSSL_SHA256 = 'aes-256-cbc'

ACTIONS = [ENCRYPT, DECRYPT]


class FileNameError(Exception):
    """Extend Exception to provide multiple reasons for this error."""

    def __init__(self, errors, *args, **kwargs):
        self.message = f"There are errors in your filenames."
        super().__init__(self.message, *args, **kwargs)
        self.errors = errors


def get_arguments():
    """Get required arguments."""
    parser = ArgumentParser(description=str(__doc__))
    parser.add_argument('action', choices=ACTIONS,
                        help=f"One of: {', '.join(ACTIONS)}")
    parser.add_argument('input', help=f"(En/De)crypt this input file.")
    parser.add_argument('--output', default=None,
                        help="the results of the action will be written here.")
    return parser.parse_args()


def validate_inputs(action, input_file, output_file, **kwargs):
    """Format the input and output files depending on the action.

    * for encryption, expect that the input file does not have an extenstion.
      The output of the file should be named the same as the input but
      concatenated with .enc
    * for decryption, the input file should be .enc and the output file should
      not have the .enc extension.

    Raises:
        * Excpetion if encrypting a file with <extension>
        * Exception if decrypting a file without <extension>
        * Exception if trying to decrypt to a file ending in <extension>

    Returns formatted input and output files.
    """
    encrypt_extension = kwargs.get('encryption_extension', '.enc')
    decrypt_extension = kwargs.get('decrypt_extension', '.env')
    errors = kwargs.get('errors', {})
    raise_exception = kwargs.get('raise_exception', True)
    drop_filename = kwargs.get('drop_filename', True)

    # in case that os.path.split returns none as the filename.
    if output_file and output_file == 'None':
        output_file = None

    if action == ENCRYPT:
        if encrypt_extension in input_file:
            errors['encrypt_input'] = (f"You are trying to encrypt a file that"
                                       f" may already be encrypted. check "
                                       f"extention '{encrypt_extension}'")
        if output_file and encrypt_extension not in output_file:
            output_file = f"{output_file}{encrypt_extension}"
        if not output_file:
            output_file = f"{input_file.split('.')[0]}{encrypt_extension}"
    if action == DECRYPT:
        if encrypt_extension not in input_file:
            errors['decrypt_input'] = (f"You are trying to decrypt a file that"
                                       f" may not be encrypted. '{input_file}'"
                                       f" does not end in "
                                       f"'{encrypt_extension}'.")
        if output_file and encrypt_extension in output_file:
            errors['decrypt_output'] = (f"After decryption, contents will be "
                                        f"written to a file ending in "
                                        f"'{encrypt_extension}'. Your "
                                        f"output_file is not allowed to end in"
                                        f" '{encrypt_extension}'.")
        if not output_file:
            if drop_filename:
                output_file = decrypt_extension
            else:
                output_file = f"{input_file.split('.')[0]}{decrypt_extension}"

    if errors and raise_exception:
        raise FileNameError(errors)

    return (input_file, output_file, errors)


def encrypt_decrypt(action, input_file, output_file, algorithm=SHA256,
                    openssl_algorithm=OPENSSL_SHA256):
    """decrypt a file and source the created file.

    Arguments:
        * action
        * filename (str): absolute path to the file to decrypt.
        * output (str): absolute path to the output folder. If not given, a
          .env file will be created containing the variables.
        * algorithm (str): string representation of the algorithm used.

    WARNING:
        Environment variables are not UNset...
    """
    input_dir, input_filename = path.split(input_file)
    output_dir, output_filename = path.split(output_file)
    try:
        input, output, errors = validate_inputs(action,
                                                input_filename,
                                                output_filename)
    except FileNameError as ex:
        [print(f"{code}: {detail}") for code, detail in ex.errors.items()]
        raise ex
    input_file = path.join(input_dir, input)
    output_file = path.join(output_dir, output)
    password = bytes(getpass(), 'utf-8')
    salted_password = hashlib.new(algorithm, password).hexdigest()

    command_list = [
        'openssl',
        f'{openssl_algorithm}',
        f'{"" if action == ENCRYPT else "-d"}',
        f'-k {salted_password}',
        f"-in {input_file}",
        f"-out {output_file}",
        ]
    try:
        subprocess.Popen(" ".join(command_list), shell=True)
    except Exception:
        raise Exception(f"Got an unexpected error while {action}.")


if __name__ == '__main__':
    """Entry point."""
    args = get_arguments()
    input = path.abspath(f"{args.input}")
    output = path.abspath(f"{args.output}")
    # ensuring the correct files are being passed to the function depending on
    # action.
    encrypt_decrypt(args.action, input, output)
    exit(f"Your file has been {args.action}")
